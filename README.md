# Fast Monitoring Client

This repo provides a python module (FastMonitoringClient) that helps in interacting with projects implementing [SpyBuffers](https://gitlab.cern.ch/spybuffer/spybuffer).

## Requirements and assumptions
The FastMonitoringClient is based on IPbus uHAL, from [ipbus.web.cern.ch](https://ipbus.web.cern.ch). All the SpyBuffer memories are expected to be mapped as in [FM.xml](./extra/uhal_dummy/modules/FM.xml) according to uHAL.

A Fast Monitoring block is expected to be implemented in the firmware, defining global FREEZE and PLAYBACK signals, as well as masks for defining which SpyBuffers are affected by the global signals.

## Quick start instructions
1. Clone this repo and add its root folder to the `PYTHONPATH` environment variable
2  Set-up your hardware with uHAL, or use the [dummy hardware emulator provided by IPbus](https://ipbus.web.cern.ch/doc/user/html/software/uhalQuickTutorial.html?highlight=dummy#working-with-dummy-hardware)
3. Check the examples in the [test](./FastMonitoringClient/test/) directory





