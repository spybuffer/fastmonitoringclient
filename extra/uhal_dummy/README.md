# How to run dummy uhal device
```
export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
export PATH=/opt/cactus/bin:$PATH
```

# To start the server
```
/opt/cactus/bin/uhal/tests/DummyHardwareUdp.exe -p 50001 -v 2 &
```