# Simple interface for a HW device 
# implementing functions needed by FMClient
# by wrapping some of the methods of uhal::Node
#
#
# Author: Iacopo Longarinig (UC Irvine)
# Tested on the dummy uhal hardware
# 

import uhal

class HwDevice:
    def __init__(self, xmlfile, deviceName):
        manager = uhal.ConnectionManager(xmlfile)
        self.hw = manager.getDevice(deviceName)

    def Read(self,reg):
        val = self.hw.getNode(reg).read()
        self.hw.dispatch()
        return val

    def ReadBlock(self,reg,n):
        val = self.hw.getNode(reg).readBlock(n)
        self.hw.dispatch()
        return val
    
    def Write(self,reg,val):
        self.hw.getNode(reg).write(val)
        self.hw.dispatch()

    def WriteBlock(self,reg,valVec):
        self.hw.getNode(reg).writeBlock(valVec)
        self.hw.dispatch()

    def GetRegDescription(self,reg):
        return self.hw.getNode(reg).getDescription()
    def GetRegMode(self,reg):
        return self.hw.getNode(reg).getMode()
    def GetRegPermissions(self,reg):
        return self.hw.getNode(reg).getPermission()
    def GetRegAddress(self,reg):
        return self.hw.getNode(reg).getAddress()
    def GetRegMask(self,reg):
        return self.hw.getNode(reg).getMask()
    def GetRegSize(self,reg):
        return self.hw.getNode(reg).getSize()



###
### For testing purposes
### paths are relative to the root of this repo
if __name__ == "__main__":
    xmlfile = "file://extra/uhal_dummy/connections.xml"
    deviceName = "dummy.udp.0"
    
    dev = HwDevice(xmlfile, deviceName)

    dev.Write("FM.SB_DUMMY0.SB_MEM",1)
    print(dev.Read("FM.SB_DUMMY0.SB_MEM"))

    dev.WriteBlock("FM.SB_DUMMY0.SB_MEM",[0xabcd,0xcaffee])
    print([hex(x) for x in dev.ReadBlock("FM.SB_DUMMY0.SB_MEM",2)])
    

    print("GetRegDescription(): ", dev.GetRegDescription("FM.SB_DUMMY0.SB_MEM"))
    print("GetRegMode(): ", dev.GetRegMode("FM.SB_DUMMY0.SB_MEM"))
    print("GetRegPermissions(): ", dev.GetRegPermissions("FM.SB_DUMMY0.SB_MEM"))
    print("GetRegAddress(): ", dev.GetRegAddress("FM.SB_DUMMY0.SB_MEM"))
    print("GetRegMask(): ", dev.GetRegMask("FM.SB_DUMMY0.SB_MEM"))
    print("GetRegSize(): ", dev.GetRegSize("FM.SB_DUMMY0.SB_MEM"))
