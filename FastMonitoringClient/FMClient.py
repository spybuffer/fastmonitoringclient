## Fast Monitoring Client
## author: Iacopo Longarini (UC Irvine)

from FastMonitoringClient.SpyBufferMgr import SBcontainer
from extra.UhalDevice import HwDevice
from FastMonitoringClient.utils.SpyBufferUtils import ReadSpyBuffersFromXML, ReadMasksFromXML
from FastMonitoringClient.utils.SpyBufferUtils import PlaybackMode, FreezeMode
from tabulate import tabulate

import math
import re
import logging

def printword(bitWord):
    return hex(bitWord)
    #return bin(bitWord)


class FMClient(HwDevice,SBcontainer):

    def __init__(self,connectionFile, deviceName):        
        HwDevice.__init__(self,connectionFile, deviceName)        
        SBcontainer.__init__(self)
        self.mask={}
        self.mode2enum = {"FREEZE" : FreezeMode, "PLAYBACK" : PlaybackMode}

    def Config(self,xmlFile,doReset=True):
        logging.getLogger(__name__).debug(f"Calling Config with arguments: {xmlFile},{doReset}")

        # Create SB objects
        ReadSpyBuffersFromXML(self,xmlFile)

        ReadMasksFromXML(self,xmlFile)

        if doReset:
            self.Reset()
        else:
            self.ReadMode()
        
        
        logging.getLogger(__name__).info("FM initialized... Dumping status:")
        self.Report()
        

    def SetMode(self,SBlist,mode,modeOption):
        """
        Main method for setting Freeze or Playback mode to SB
        arguments:
          SBlist     : list of SB on which to apply mode
          mode       : str, either "FREEZE" or "PLAYBACK"
          modeOption : depends on mode:
                        -  mode == "FREEZE" => FreezeMode.OFF or FreezeMode.ON
                        -  mode == "PLAYBACK" => one item of PlaybackMode Enum
        """

        if SBlist=="*":
            SBlist = list(self.SpyBuffers.keys())
        

        if ( (mode == "FREEZE" and type(modeOption) is not FreezeMode) or
             (mode == "PLAYBACK" and type(modeOption) is not PlaybackMode)):
            raise TypeError(f"Was trying to set mode {mode} with type(modeOption): "+str(type(modeOption)))
        
        
        ### Previous variant
        ### keeping it for reference
        # self.WriteMask(mode,SBlist)
        # self.WriteMode(mode,modeOption)

        ### Current variant: supports the following case.
        ### (SB respond to mask even if global control is unchanged)
        ### Let's say SB1 is masked (bit is 0) and in "FREEZE" state.
        ### Un-Masking it will automatically imply a transition to "NON-FREEZE"
        
        ### Update local mode register        
        isChanged=False
        for SBid, SB in self.SpyBuffers.items():
            if SBid in SBlist and SB.mode[mode] != modeOption:
                SB.mode[mode]=modeOption
                isChanged=True

        if not isChanged:
            logging.getLogger(__name__).info(f"Request ignored, all SB of list: {SBlist} are already in mode {mode} : {modeOption.name}.")
        else:
            ### Update local mask from what stored in 'mode'
            n_SB_masked=0
            for SBid, SB in self.SpyBuffers.items():
                if self.mode2enum[mode](SB.mode[mode])!=self.mode2enum[mode](0) and self.mode2enum[mode](SB.mode[mode])!=self.mode2enum[mode](-1):
                    ### Need to handle unknown!!!
                    logging.getLogger(__name__).debug(f"Setting SB {SBid} mask {mode} to 0 as it's {self.mode2enum[mode](SB.mode[mode])}")
                    SB.mask[mode]=0
                    n_SB_masked+=1
                else:
                    ### This sets either FreezeMode.OFF or PlaybackMode.NO_PLAYBACK
                    SB.mask[mode]=1

            ### Recalculate mask and write to registers
            self.RecalculateMask()
            for maskName,maskValue in self.mask[mode].items():
                logging.getLogger(__name__).debug("Writing local mask "+str(printword(maskValue))+" on reg FM."+maskName)
                self.Write("FM."+maskName,maskValue)

            ### Check when to write global mode
            doWriteMode=True
            if self.mode2enum[mode](modeOption)==self.mode2enum[mode](0) and n_SB_masked>0 :                    
                ### Prevent writing 0 to SPY_CTRL.GLOBAL when there are SB that should not be affected
                doWriteMode=False
                ### Should also include case where global ctrl would be overwritten
                ### not done for now (not sure if things should work like this)

            if doWriteMode:
                if mode=="PLAYBACK":
                    mode="PLAYBACK_MODE"
                ### Write the GLOBAL mode register
                logging.getLogger(__name__).info("Setting SPY_CTRL.GLOBAL_"+mode+" set to "+modeOption.name)
                self.Write("FM.SPY_CTRL.GLOBAL_"+mode,modeOption.value)
        

    def WriteMask(self,mode,SBlist):
        """
        Updates local instance of the mask for the given mode and
        for the given SpyBuffers. Then writes the mask to the FM block register.
        Refer to SetMode for arguments description.
        """
        logging.getLogger(__name__).debug(f"WriteMask({mode},{SBlist})")

        ### Update mask info of each SB
        for SBid, SB in self.SpyBuffers.items():        
            if SBid in SBlist:
                SB.mask[mode] = 0
                logging.getLogger(__name__).debug(f"Local masking info on SB updated: {SBid}.mask[{mode}]={SB.mask[mode]}")
            #else:
                # This following one changes the behaviour of the WriteMask function
                # if uncommented, every call to SetMode un-does the previous calls
                # So, if a SB was frozen and now a call is made to froze a different SB, then 
                # in such case the SB will be un-frozen
                # SB.mask[mode] = 1 ### will set corresponding bit to 1 (masked)
                
        
        ### Re-compute mask from SB information
        self.RecalculateMask()

        ### Write mask on register
        for maskName,maskValue in self.mask[mode].items():
            logging.getLogger(__name__).debug("Writing local mask "+str(printword(maskValue))+" on reg FM."+maskName)
            self.Write("FM."+maskName,maskValue)
                    

    def WriteMode(self, mode, modeOption):
        """
        Check that a new mode is about to be set, then 
        write to global register controlling the mode.
        """
        logging.getLogger(__name__).debug("In WriteMode()")

        ### Check if SB status will be unchanged
        n_sb_upd = 0
        for SBID, SB in self.SpyBuffers.items():
            if SB.mask[mode] == 0:
                if SB.mode[mode] != modeOption:
                    n_sb_upd+=1
                    logging.getLogger(__name__).info(f"Setting SB {SBID} mode {mode} to {modeOption.name}")
                    SB.mode[mode]=modeOption
                else:
                    logging.getLogger(__name__).warning(f"{SBID} is already in mode {mode} : {modeOption.name}, this will not make any difference")
                    fixMask=True

        if n_sb_upd == 0:
            logging.getLogger(__name__).warning(f"Nothing will be changed when setting {mode} {modeOption.name}")
        else:
            ### Temporary? Unifromize name of registers
            if mode=="PLAYBACK":
                mode="PLAYBACK_MODE"

            ### Write the GLOBAL mode register
            logging.getLogger(__name__).info("Setting SPY_CTRL.GLOBAL_"+mode+" set to "+modeOption.name)
            self.Write("FM.SPY_CTRL.GLOBAL_"+mode,modeOption.value)
            
                        
       
    def RecalculateMask(self):
        """
        Updates local mask using information stored in SB
        masks are 32-bit wide, so max 32 SB per mask are supported
        """
        logging.getLogger(__name__).debug("In RecalculateMask()")
        for mode in ['FREEZE','PLAYBACK']:
            for iSB, (SBID, SB) in enumerate(self.SpyBuffers.items()):
                iMask = math.floor(iSB/32)
                if SB.mask[mode]:
                    self.mask[mode][mode+"_MASK_"+str(iMask)] = self.mask[mode][mode+"_MASK_"+str(iMask)]  | (1 << (iSB % 32))
                else:
                    self.mask[mode][mode+"_MASK_"+str(iMask)] = self.mask[mode][mode+"_MASK_"+str(iMask)]  & ~(1 << (iSB % 32))
                #logging.getLogger(__name__).debug(f"Mask {mode} for SB {SBID} about to be set to {(self.mask[mode][mode+'_MASK_'+str(iMask)] >> (iSB%32) ) & 1 }")
                                
        if logging.getLogger(__name__).getEffectiveLevel() == logging.DEBUG:
            logging.getLogger(__name__).debug("Local masks re-computed from local info")
            for mode in ['FREEZE','PLAYBACK']:
                for mask, value in self.mask[mode].items():
                    logging.getLogger(__name__).debug(mask +" : "+ printword(value))

    def ReadMask(self):
        logging.getLogger(__name__).debug("In ReadMask()")

        for mode in ['FREEZE','PLAYBACK']:
            for maskName in self.mask[mode].keys():
                thisMask = self.Read("FM."+maskName)
                self.mask[mode][maskName] = thisMask
                        
                ### Now update existing SB
                iMask = int(re.sub(r".*_MASK_([0-9]+)",r"\1",maskName))
                for iSB, (SBID, SB) in enumerate(self.SpyBuffers.items()):
                    if math.floor(iSB/32) == iMask:
                        SB.mask[mode] = (thisMask >> (iSB%32)) & 1
                

                logging.getLogger(__name__).debug("Done reading and updating mask from HW "+maskName+" : "+printword(self.mask[mode][maskName]))
                
    def ReadMode(self):
        """
        Reads the masks from HW.
        Reads the status of Playback and Freeze global mode registers from HW.
        Makes guess on the status of each SB and stores it in the SW memory.
        """

        ### Read mask from HW and update info on SW 
        self.ReadMask()

        for mode in ['FREEZE','PLAYBACK']:
            mode_to_read = mode
            if mode=="PLAYBACK":
                mode_to_read="PLAYBACK_MODE"

            ### Read global mode and store it as enum object
            status = self.mode2enum[mode](int( self.Read("FM.SPY_CTRL.GLOBAL_"+mode_to_read) ))
            logging.getLogger(__name__).info("Read from HW - SPY_CTRL.GLOBAL_"+mode+" = "+status.name)

            ### Guess the status of each SB and store it 
            for SBID, SB in self.SpyBuffers.items():
                if SB.mask[mode]==1:
                    SB.mode[mode]=self.mode2enum[mode](0)
                else:
                    SB.mode[mode]=status


    def Reset(self):
        """
        Bring the FM in a known state where everything is masked,
        freeze is off and playback is off
        """
        logging.getLogger(__name__).info("Called Reset, setting everything to off")
        self.SetMode("*","FREEZE",FreezeMode.OFF)
        self.SetMode("*","PLAYBACK",PlaybackMode.NO_PLAYBACK)

    def ResetPlayback(self,SBlist):
        """
        Playback mode works correctly if the playback_write is asserred AND
        if the registers that defines the start and stop position of the playback
        operation are well defined. This Reset control re-initializes the counters, so that
        when the playback_write is asserred and the buffers are written, the counters are
        updated accordingly.        
        """
        if SBlist=="*":
            SBlist = list(self.SpyBuffers.keys())
        if type(SBlist) is not list:
            raise TypeError("Function ResetPlayback called with non-list argument")
        
            
        resetMask={}
        for iSB, (SBID, SB) in enumerate(self.SpyBuffers.items()):
                iMask = math.floor(iSB/32)
                iBit = iSB%32
                
                bitValue = (SBID in SBlist)
                if iMask not in resetMask:
                    resetMask[iMask]=0
                resetMask[iMask] |= (bitValue << iBit)
        
        for i, mask in resetMask.items():
            logging.getLogger(__name__).debug(f"Going to reset SB with mask {i} with {hex(mask)} = {bin(mask)} ")
            if mask>0:
                self.Write(f"FM.SB_RESET_{i}",mask)
                self.Write(f"FM.SB_RESET_{i}",0)
    

    def Report(self):
        header=['SB id', "width", "Mask (Freeze, Playback)", "Freeze", "Playback"]
        table = []
        for iSB, (SBID, SB) in enumerate(self.SpyBuffers.items()):
            table.append([ SBID,
                           SB.width,
                           "("+str(SB.mask['FREEZE'])+", "+str(SB.mask['PLAYBACK'])+")",
                           SB.mode['FREEZE'].name,
                           SB.mode['PLAYBACK'].name])
        logging.getLogger(__name__).info("\n"+tabulate(table,headers=header,tablefmt="pretty",colalign="left"))



    def ReadSpyMemory(self, SBID, n=None, ignoreResize=False):
        """
        Returns the content of the Spy Memory of SBID
        reading from the beginning up to register n.
        
        TO DO: Implement a variant "Read k words" that takes into account the SB width
        """        

        if n is None:
            n = self.SpyBuffers[SBID].memSize
        spy_mem_reg = "FM."+SBID+".SB_MEM"
        if (n > self.SpyBuffers[SBID].memSize):
            logging.getLogger(__name__).warning(f"Requested to read {n} words from a SB with memory of depth {self.SpyBuffers[SBID].memSize}. Only the first {self.SpyBuffers[SBID].memSize} will be read!")
            n=self.SpyBuffers[SBID].memSize
            
        vec = self.ReadBlock(spy_mem_reg, n)        

        if not ignoreResize:
            vec = self.ResizeRead(SBID,list(vec))
        return vec


    def WriteSpyMemory(self, SBID, data):
        """
        Write data to SB memory, data are written from the 1st register of the
        memory up to len(data).
        Data should be of type list (iterable to be tested).

        If len(data) is larger than the mem size the request will be ignored
        """
        spy_mem_reg = "FM."+SBID+".SB_MEM"
        data = self.ResizeWrite(SBID, data)
        
        if (len(data) > self.SpyBuffers[SBID].memSize):
            logging.getLogger(__name__).error(f"Requested to write {len(data)} words (after ResizeWrite) on a SB with memory of depth {self.SpyBuffers[SBID].memSize}. Request ignored!")
        else:
            ### Padding zeros at the end to match memSize
            data = data + [0 for _ in range(self.SpyBuffers[SBID].memSize-len(data))]
            self.WriteBlock(spy_mem_reg, data)


    def ClearSpyMemory(self, SBID):
        """
        Write 0s to SB memory, all the bits are written.
        """
        spy_mem_reg = "FM."+SBID+".SB_MEM"
        self.WriteBlock(spy_mem_reg, [0 for _ in range(self.SpyBuffers[SBID].memSize)])
        
        
    def ResizeWrite(self, SBID, data):
        """
        Splits the content of data in subsequent words of 32 bits each.
        This is done only if the spy buffer width is set to > 32 bits
        """
        nSplit = self.SpyBuffers[SBID].nSplit
        width  = self.SpyBuffers[SBID].width

        if nSplit==1:
            return data
        else:
            data_32bit = []
            for x in data:
                if x > (1<<width+1)-1:
                    raise Exception(f"Provided word {hex(x)} requires more bits than SB width {width}")
                for n in range(nSplit):
                    #-----------------------------------------
                    # Little endian (lsb is first in the list)
                    #-----------------------------------------
                    ### Take the n-th block of 32 lsb bits
                    x_trf = (((0xFFFFFFFF << 32*n) & x) >> 32*n)
                    if n==nSplit-1 and width%32 != 0:
                        ### Set the last bits of the trailing block to 0
                        x_trf = x_trf & ( 0xFFFFFFFF >> 32-(width%32) )
                    #-----------------------------------------
                    # Big endian (lsb is first in the list)
                    #-----------------------------------------
                    # if n==0:
                    #     ### Take the first block of msb (accounting for width)
                    #     x_trf = (x & ( 0xFFFFFFFF << (nSplit-1-n)*32 )) >> (nSplit-1-n)*32
                    #     print(f"write n {n} nsplit {nSplit} result {hex(x_trf)}")
                    # else:
                    #     ### Take the n-th block of 32 msb bits
                    #     print(f"write n {n} nsplit {nSplit} result {hex(x_trf)}")
                    #     x_trf = ((0xFFFFFFFF << 32*(nSplit-1-n) & x) >> 32*(nSplit-1-n))                                        
                    data_32bit.append(x_trf)
            return data_32bit

    

    def ResizeRead(self, SBID, data_32bit):
        """
        Opposite logic of ResizeWrite, takes 32bit words
        and makes larger words (if the SB has width>32)
        """
        nSplit = self.SpyBuffers[SBID].nSplit
        width  = self.SpyBuffers[SBID].width
                
        if nSplit==1:
            return data_32bit
        else:
            data=[]
            word=0            
            for idata32 in range(len(data_32bit)):                
                n = idata32 % nSplit
                #----------------------------------------
                # Little endian (lsb is first in the list)
                #----------------------------------------
                word = word | (data_32bit[idata32] << n*32)

                #----------------------------------------
                # Big endian (msb is first in the list)
                #----------------------------------------
                # word = (word << n*32) | data_32bit[idata32]
                
                if n==nSplit-1:
                    data.append(word)
                    word=0

            return data
                    
                
        
        
