import argparse
import os
import time
import logging

from FastMonitoringClient.FMClient import FMClient
from FastMonitoringClient.utils.SpyBufferUtils import PlaybackMode, FreezeMode
from FastMonitoringClient.utils.RegUtils import PrintSBmem
from FastMonitoringClient.utils.FMCutils import LoadFMClient

def getOptions():
    ap = argparse.ArgumentParser()
    ap.add_argument('connectionFile',help='Path to connection xml file')
    ap.add_argument('deviceName',help='Device name (e.g. dummy.udp.0)')
    ap.add_argument('--SB', dest='target_SB', nargs="+", help='SpyBuffer to operate')
    ap.add_argument('--noReset', action='store_true', help="If speficied, does not reset the FM at startup")
    ap.add_argument('--dropZeros', action='store_true', help="If specified, after initializing FM clears all the SB content"+
                    "and waits for the user key to be pressed, output zeros will not be printed")
    args = ap.parse_args()
    return args


if __name__ == '__main__':

    ### Command Line options, logger
    args = getOptions()
    logging.basicConfig(level=logging.INFO,
                        format = '%(asctime)s - %(levelname)s - [%(module)s] :: %(message)s'
                        )
    logging.getLogger(__name__).info(f"Called {__file__}")
   
    client = LoadFMClient(args.connectionFile,args.deviceName, args.noReset)

    
    target_SB = args.target_SB
    if target_SB is not None:
        client.Write('FM.SPY_CTRL.INITIALIZE_SPY_MEMORY',0)
        client.SetMode( target_SB,"FREEZE",FreezeMode.ON)
        for SBID in target_SB:
            client.ClearSpyMemory(SBID)
        client.SetMode( target_SB,"FREEZE",FreezeMode.OFF)

        input("press a key to continue")
        client.SetMode( target_SB,"FREEZE",FreezeMode.ON)
        for SBID in target_SB:
            PrintSBmem(client, SBID, text=f"{SBID} {client.SpyBuffers[SBID].dataformat}", dropzeros=args.dropZeros)
        client.SetMode( target_SB,"FREEZE",FreezeMode.OFF)    
    
        client.Write('FM.SPY_CTRL.INITIALIZE_SPY_MEMORY',1)
    else:
        logging.getLogger(__name__).info("No SpyBuffer specified! Leaving")
                   
    del client
    exit(0)



