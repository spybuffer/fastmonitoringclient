import argparse
import os
import logging
from FastMonitoringClient.utils.SpyBufferUtils import PlaybackMode, FreezeMode
from FastMonitoringClient.utils.FMCutils import LoadFMClient
def getOptions():
    ap = argparse.ArgumentParser()
    ap.add_argument('connectionFile',help='Path to connection xml file')
    ap.add_argument('deviceName',help='Device name (e.g. dummy.udp.0)')
    ap.add_argument('--noReset', action='store_false', help="If speficied, does not reset the FM at startup")
    ap.add_argument('--SBname', help='SpyBuffer to operate')
    
    args = ap.parse_args()
    return args



if __name__ == '__main__':

    ### Command Line options, logger
    args = getOptions()
    logging.basicConfig(level=logging.DEBUG,
                        format = '%(asctime)s - %(levelname)s - [%(module)s] :: %(message)s'
                        )
    logging.getLogger(__name__).info(f"Called {__file__}")

    
    client = LoadFMClient(args.connectionFile,args.deviceName,args.noReset,loglevel="DEBUG")
    
    ####
    ## Do your stuff here
    ####
    
    del client
    exit(0)

