import argparse
import os
import time
import logging

from FastMonitoringClient.FMClient import FMClient
from FastMonitoringClient.utils.SpyBufferUtils import PlaybackMode, FreezeMode
from FastMonitoringClient.utils.RegUtils import PrintSBmem
from FastMonitoringClient.utils.FMCutils import LoadFMClient

def getOptions():
    ap = argparse.ArgumentParser()
    ap.add_argument('connectionFile',help='Path to connection xml file')
    ap.add_argument('deviceName',help='Device name (e.g. dummy.udp.0)')
    ap.add_argument('--noReset', action='store_true', help="If speficied, does not reset the FM at startup")
    
    args = ap.parse_args()
    return args




if __name__ == '__main__':

    ### Command Line options, logger
    args = getOptions()
    logging.basicConfig(level=logging.INFO,
                        format = '%(asctime)s - %(levelname)s - [%(module)s] :: %(message)s'
                        )
    logging.getLogger(__name__).info(f"Called {__file__}")
   
    client = LoadFMClient(args.connectionFile,args.deviceName, args.noReset)

    client.Write('FM.SPY_CTRL.INITIALIZE_SPY_MEMORY',0)
    
    
    SB_DUMMY0="SB_DUMMY0"
    SB_DUMMY1="SB_DUMMY1"
    SB_DUMMY0_width = client.SpyBuffers[SB_DUMMY0].width
    data=[(0xCAFFEE00) + i for i in range(10)]

    client.SetMode( [SB_DUMMY0],"FREEZE",FreezeMode.ON)

    client.ClearSpyMemory(SB_DUMMY0)
    client.ResetPlayback([SB_DUMMY0])
    client.SetMode( [SB_DUMMY0],"PLAYBACK",PlaybackMode.PLAYBACK_WRITE)
    client.ResetPlayback([SB_DUMMY1])
    client.ClearSpyMemory(SB_DUMMY1)
    
    client.WriteSpyMemory(SB_DUMMY0, data)

    logging.getLogger(__name__).info("Printing SB memories before playback")
    PrintSBmem(client, SB_DUMMY0, text="SB_DUMMY0")
    PrintSBmem(client, SB_DUMMY1, text="SB_DUMMY1")


    client.SetMode( [SB_DUMMY0],"PLAYBACK",PlaybackMode.PLAYBACK_ONCE)
    #client.SetMode( [SB_DUMMY0],"PLAYBACK",PlaybackMode.NO_PLAYBACK)

    logging.getLogger(__name__).info("Printing SB memories AFTER playback")
    PrintSBmem(client, SB_DUMMY0, text="SB_DUMMY0")
    PrintSBmem(client, SB_DUMMY1, text="SB_DUMMY1")

    client.SetMode("*","FREEZE",FreezeMode.OFF)
    client.SetMode("*","PLAYBACK",PlaybackMode.NO_PLAYBACK)
    client.Write('FM.SPY_CTRL.INITIALIZE_SPY_MEMORY',1)
                   
    del client
    exit(0)

