# Example of SpyBuffer tests scripts

## [FastMonitoringStatus.py](./FastMonitoringStatus.py)
This script sets up the FM client and prints a table with SpyBuffers and their status (e.g. which SB are registered, info if they are in freeze / playback mode), see example below:
```
+-----------+--------+-------------------------+--------+-------------+
|     SB id | width  | Mask (Freeze, Playback) | Freeze |  Playback   |
+-----------+--------+-------------------------+--------+-------------+
| SB_DUMMY0 | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB_DUMMY1 | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB1       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB4       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB7       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB10      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB13      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB16      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB19      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB22      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB25      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB0       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB2       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB3       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB5       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB6       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB8       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB9       | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB11      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB12      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB14      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB15      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB17      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB18      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB20      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB21      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB23      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB24      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
| SB26      | 32     | (1, 1)                  |  OFF   | NO_PLAYBACK |
+-----------+--------+-------------------------+--------+-------------+
```
## [Freeze_and_read.py](./Freeze_and_read.py)

This script executes the Freeze command on a list of SpyBuffers and prints their content
```
usage: Freeze_and_read.py [-h] [--SB TARGET_SB [TARGET_SB ...]] [--noReset] [--dropZeros] connectionFile deviceName

positional arguments:
  connectionFile        Path to connection xml file
  deviceName            Device name (e.g. dummy.udp.0)

optional arguments:
  -h, --help            show this help message and exit
  --SB TARGET_SB [TARGET_SB ...]
                        SpyBuffer to operate
  --noReset             If speficied, does not reset the FM at startup
  --dropZeros           If specified, after initializing FM clears all the SB contentand waits for the user key to be
                        pressed, output zeros will not be printed
```

## [Test_dummy_FM_block.py](./Test_dummy_FM_block.py)
This scripts runs the example mentioned in [this presentation](https://indico.cern.ch/event/1381060/contributions/5923210/)

If this is run on the uHAL dummy hardware, since no firmware with SpyBuffer is used, the 2nd SpyBuffers will be empty!

