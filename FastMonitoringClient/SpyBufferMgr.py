"""
Defines two classes

SBcontainer : stores SBs and performs coordinated global freeze and read
             

SB          : Represents a SB
              
"""

import sys
import logging
import math
from FastMonitoringClient.utils.SpyBufferUtils import MaskingMode, PlaybackMode, FreezeMode


class SB:

    def __init__(self,sbID,address,memSize,width,df):
        self.address = address
        self.id = sbID
        self.memSize=memSize
        self.width = width
        self.nSplit = math.ceil(width/32)
        self.actualSize=math.floor(memSize/self.nSplit)
        self.dataformat=df
        
        self.mask = {'FREEZE': 0 ,'PLAYBACK' : 0}
        self.mode = {'FREEZE': FreezeMode.UNKNOWN ,'PLAYBACK' : PlaybackMode.UNKNOWN }
    
    def __str__(self):
        return f"(SpyBuffer id: {self.id}, address: {self.address})"


        
class SBcontainer:

    def __init__(self):
        logging.getLogger(__name__).debug("Creating new SBcontainer")
        self.SpyBuffers={}


    def AddSB(self,sbID,address,memSize,width,df):            
        if width!=32:
            width=int(width)
            logging.getLogger(__name__).info(f"SpyBuffer {sbID} has width set to {width}")
        

        spy = SB(sbID,address,memSize,width,df)

        
        self.SpyBuffers[sbID]=(spy)
        logging.getLogger(__name__).info(f"Added new spybuffer: {spy}")
