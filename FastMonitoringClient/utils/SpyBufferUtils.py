from xml.etree import ElementTree
from uhal import ValWord_uint32
import logging
from enum import Enum
import re
import os

### Enum for PlaybackMode
class PlaybackMode(Enum):
    UNKNOWN=-1
    NO_PLAYBACK=0
    PLAYBACK_ONCE=1
    PLAYBACK_LOOP=2
    PLAYBACK_WRITE=3
    
class FreezeMode(Enum):
    UNKNOWN=-1
    OFF=0
    ON=1

class MaskingMode(Enum):
    FREEZE=0
    PLAYBACK_MODE=1

def ReadXML(xmlFile):
    """
    Basic function for processing XML files for L0MDT FW
    
    Navigates through the XML files and reads it with xml.etree.ElementTree
    If a line has an attribute such as:  module="file://modules/FM.xml"
    the module file is searched and its content is also processed (recursively)    
    """

    if not os.path.isfile(xmlFile):
        raise Exception(f"File not found: {xmlFile}")

    ### Read tree from XML
    ET = ElementTree.parse(xmlFile)
    root = ET.getroot()

    ### Check for the presence of sub-trees and replace 
    ### the block pointing to the file with the file content
    child_nodes = list(root)
    for ix, node in enumerate(child_nodes):
        if "module" in  node.attrib.keys():
            subfile = os.path.join(os.path.dirname(xmlFile),
                                   re.sub(r"file://(.*\.xml)",r"\1",node.attrib['module']))
            if not os.path.isfile(subfile):
                raise Exception(f"Unable to find {subfile}")
            
            root.remove(node)
            root.insert(ix,ReadXML(subfile))

    return root

def DumpXMLTree(tree):
    """ 
    Recursive function to print L0MDT FW XML files.
    """
    for i in range(len(tree)):
        print(tree[i].attrib)
        DumpXMLTree(tree[i])

def GetFMtreeFromXML(xmlFile):
    """
    Opens a XML file and returns the FM subtree
    an exception is raised if not found
    """
    root = ReadXML(xmlFile)

    FMroot = None

    for subtree in root:
        if subtree.attrib['id']=='FM':
            FMroot = subtree

    if FMroot is None:
        raise Exception(f"Was not able to find FM subtree in xml file named {xmlFile}")
    
    return FMroot

def ReadSpyBuffersFromXML(SBcontainer,xmlFile):
    """
    Helper function for class SpyBufferContainer
    
    Opens a XML file and loads the information of the SpyBuffers
    found under the "FM" branch
    """
    FMroot = GetFMtreeFromXML(xmlFile)
    
    ### Loop over XML tree and create a list of the SB
    list_SB=[]
    for node in FMroot:
        if node.attrib["id"].startswith("SB"):            
            if node.attrib["id"].startswith("SB_RESET"):            
                continue
            list_SB.append(node.attrib['id'])

    if len(list_SB) is 0:
        raise Exception("No SpyBuffers found in FM subtree, please check input files!")


    # ### Re-strip leading zeros
    # list_SB_new = [ re.sub(r'SB00([0-9])',r'SB\1',SB) for SB in list_SB_new ]  
    # list_SB = list_SB_new
    ### Navigate through nodes and add SB
    for SB_id in list_SB:
        for node in FMroot:
            if node.attrib['id'] == SB_id:
                ### Default values
                width=32
                memSize=32
                df=''
                ### Parse info from node description
                if "description" in node.attrib:
                    desc=ParseSBdescription(node.attrib['description'])
                    width = desc['width']
                    df=desc['df']
                ### Parse info from SB_MEM
                for subnode in node:
                    if subnode.attrib['id']=="SB_MEM":
                        memSize=int(subnode.attrib["size"],16)                        

                SBcontainer.AddSB(node.attrib["id"], node.attrib["address"], memSize, width, df)           
    

def ParseSBdescription(description):
    """
    Parse the content of the string "description" in the XML file
    arguments can be comma-separated and must be passed as key=value
    """
    args={}
    for i,substr in enumerate(description.strip().split(" ")):
        if not "=" in substr:
            raise Exception(f"description field in FM xml file is not formatted properly, The {i}-th substring is : {substr}")
        else:
            key, value = substr.split("=")
            args[key]=value
    return args
    
                

def ReadMasksFromXML(FMClient,xmlFile):
    """
    Helper function for FMClient
    
    Opens a XML file and loads the FM masks
    found under the "FM" branch
    """

    FMroot = GetFMtreeFromXML(xmlFile)

    for node in FMroot:
        if "MASK" in node.attrib['id']:
            mode = re.sub(r"(.*)_MASK_.*",r"\1",node.attrib['id'])
            if mode not in FMClient.mask.keys():
                FMClient.mask[mode]={}
            FMClient.mask[mode][node.attrib['id']]=int(node.attrib['mask'],16)
            logging.getLogger(__name__).debug(f"Reading mask from XML {node.attrib['id']} : {node.attrib['mask']}")
    logging.getLogger(__name__).debug(f"Printing FMClient.mask (as integer) {FMClient.mask}")
            
