from pprint import pprint
import shutil

def ReadAndFormatSBmem(client,regname,n=None,text=None, ignoreResize=False, dropzeros=False):
    """
    client : instance of FMClient
    regname : register to be read
    n : n. of words to be read (will read the full mem if None)
    text : string to print instead of reg name
    ignoreResize: ignores the resize during read
    """

    outStr = ""
    if not regname in client.SpyBuffers:
        raise Exception(f"Reg name {regname} not found")

    if n is None:    
        n = client.SpyBuffers[regname].memSize
    values=client.ReadSpyMemory(regname,n,ignoreResize=ignoreResize)
    if dropzeros:
        values = [ v for v in values if v != 0 ]
    if text is None:
        text=regname
    outStr+=f"{text:<25}"
    i=0    
    for value in values:
        outStr+=f"{value:#X}"+"  "
        if i==7:
            i=0
            outStr+="\n"+" "*25
        else:
            i+=1
    if len(values)==0:
        outStr+= " empty"

    outStr+="\n"
    return outStr


def PrintSBmem(client,regname,n=None,text=None, ignoreResize=False, dropzeros=False):
    """
    proxy function to print the result from ReadAndFormatSBmem
    see that function for argument defaults
    """
    print(ReadAndFormatSBmem(client,regname,n,text,ignoreResize,dropzeros=dropzeros),flush=True)


def remove_trailing_zeros(values):
    while values[-1]==0 and len(values)>1:
        del values[-1]
    return values


