from FastMonitoringClient import FMClient
from FastMonitoringClient import SpyBufferMgr
import logging 

def LoadFMClient(connectionFile, deviceName, noReset=False, loglevel="WARNING"):
    try:
        logging.getLogger(FMClient.__name__).setLevel(loglevel)
        logging.getLogger(SpyBufferMgr.__name__).setLevel(loglevel)

        client = FMClient.FMClient(connectionFile,deviceName)
        doReset = not noReset
        client.Config(connectionFile.replace("connections","dummy_address").replace("file://",""),doReset)
        return client
    except Exception as e:
        logging.getLogger(__name__).fatal(f"Catched exception when creating FMClient: {e}")
        exit(-1)
    
    
        
